import { makeAutoObservable } from "mobx";
import { makePersistable } from "mobx-persist-store";

class Store {
  constructor() {
    makeAutoObservable(this);

    makePersistable(this, {
      name: "authStore",
      properties: ["isAuth", "userData", "token", "user_role"],
      storage: window.localStorage,
    });
  }

  isAuth = false;
  userData = {};
  token = "";
  user_role = "";

  setIsAuth(value) {
    this.isAuth = value;
  }
  setUserData(data) {
    this.userData = data;
  }

  login(data) {
    this.isAuth = true;
    this.userData = data;
    this.token = data.accessToken;
  }
  updateUserRole(data) {
    this.user_role = data;
  }

  logout() {
    this.isAuth = false;
    this.userData = {};
    this.user_role = "";
    this.token = "";
  }
}

const authStore = new Store();
export default authStore;
