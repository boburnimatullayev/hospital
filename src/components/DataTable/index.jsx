/* eslint-disable react/prop-types */
import {
  background,
  Box,
  Button,
  Flex,
  Icon,
  Stack,
  Text,
} from "@chakra-ui/react";

import Pagination from "rc-pagination";
import Table from "rc-table";
import Filters from "./Filters";
import "./styles/pagination.scss";
import { HiMiniMagnifyingGlass } from "react-icons/hi2";
import { useMemo } from "react";
import SimpleLoader from "../Loaders/SimpleLoader";
import { AddIcon } from "@chakra-ui/icons";

const DataTable = ({
  columns = [],
  data = [],
  scroll,
  pagination,
  isLoading = false,
  ...props
}) => {
  const filterRowIsVisible = useMemo(() => {
    return columns.some((column) => column.filterType);
  }, [columns]);

  return (
    <>
      <Box position="relative" borderRight="1px solid" borderColor="border">
        {isLoading && (
          <SimpleLoader
            position="absolute"
            top={"1px"}
            left={"1px"}
            h="calc(100% - 2px)"
            w="calc(100% - 2px)"
            zIndex={100}
            bg="white"
          />
        )}

        <Table
          style={{ height: "80vh" }}
          columns={columns}
          data={data}
          pagination={true}
          scroll={scroll}
          // onRow={(record) => ({
          // onClick: () => navigate(`/bots/${record?.id}`),
          // })}
          emptyText={
            <Stack
              mt="100px"
              display="flex"
              alignItems="center"
              textAlign="center"
              p={10}
              spacing={1}
            >
              <Box 
              sx={{
                  borderRadius: "50%",
                  bg: "#F9f6fe",
                  p: "20px",
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center",
                }}
              >
                <Box
                  sx={{
                    borderRadius: "50%",
                    bg: "#F4EBFF",
                    p: "13px",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <Icon
                    as={HiMiniMagnifyingGlass}
                    boxSize={8}
                    color="#4d4aea"
                  />
                </Box>
              </Box>
              <Text fontSize="md" color="#101828" fontWeight={600}>
                Hech qanday ma'lumot topilmadi
              </Text>
              <Text w="352px" textAlign="center" whiteSpace="wrap" mb="15px">
                Kiritilgan filter bo‘yicha hech qanday ma’lumot topilmadi.
                Iltimos, qayta urinib ko'ring yoki yangi bemor qo'shing
              </Text>
              <Box display="flex" gap="10px">
                <Button
                  border="1px solid #ECECEE"
                  bg="#fff"
                  color="#101828"
                  _hover={{ bg: "#fff" }}
                  _active={{ transform: "translateY(1px)" }}
                >
                  Qidiruvni tozalash
                </Button>
                <Button
                  _hover={{ bg: "#5755e6" }}
                  _active={{ transform: "translateY(1px)" }}
                  sx={{
                    display: "flex",
                    gap: "12px",
                    borderRadius: "8px",
                    height: "40px",
                    width: "190px",
                    background: "#4D4AEA",
                  }}
                >
                  <AddIcon /> Bemor qo'shish
                </Button>
              </Box>
            </Stack>
          }
          summary={() =>
            filterRowIsVisible && (
              <Table.Summary fixed="top">
                <Table.Summary.Row>
                  {columns.map((column, index) => (
                    <Table.Summary.Cell key={index}>
                      <Filters
                        filterType={column.filterType}
                        placeholder={column.title}
                      />
                    </Table.Summary.Cell>
                  ))}
                </Table.Summary.Row>
              </Table.Summary>
            )
          }
          {...props}
        />
      </Box>

      <Flex pt={2} justifyContent="flex-end">
        <Pagination total={50} current={2} {...pagination} />
      </Flex>
    </>
  );
};
export default DataTable;
