import { useSortable } from "@dnd-kit/sortable";
import { CSS } from "@dnd-kit/utilities";
import  styles from './style.module.scss';
import { DragIcon } from "assets/icon";

const Drag = ({ id, title }) => {
    const { attributes, listeners, setNodeRef, transform, transition } =
    useSortable({ id });
    const style = {
        transition,
        transform: CSS.Transform.toString(transform),
      };
  return (
    <div
    ref={setNodeRef}
    style={style}
    {...attributes}
    {...listeners}
    className={styles.dragItem}
  >

    <p>{title}</p>  <DragIcon />
  </div>
  )
}

export default Drag