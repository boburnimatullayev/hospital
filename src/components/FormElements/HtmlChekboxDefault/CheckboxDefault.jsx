import styles from "./styles.module.scss";

const CheckboxDefault = ({ onChange, isChecked, props }) => {
  return (
    <div className={styles.wrap}>
      <input defaultChecked={isChecked} onChange={onChange} {...props} type="checkbox" />
      <div className={styles.checkboxDiv}></div>
    </div>
  );
};

export default CheckboxDefault;
