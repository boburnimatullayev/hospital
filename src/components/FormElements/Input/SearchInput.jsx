/* eslint-disable react/prop-types */
import { SearchIcon } from "@chakra-ui/icons";
import { Input, InputGroup, InputLeftElement } from "@chakra-ui/react";

const SearchInput = ({
  placeholder = "Search...",
  visibleSearchInput,
  ...props
}) => {
  return (
    <InputGroup display={visibleSearchInput ? "block" : "none"}>
      <InputLeftElement
        pointerEvents="none"
        // ! should be fixed
        // eslint-disable-next-line react/no-children-prop
        children={<SearchIcon color="gray" boxSize={5} />}
      />
      <Input type="text" placeholder={placeholder} {...props} />
    </InputGroup>
  );
};

export default SearchInput;
