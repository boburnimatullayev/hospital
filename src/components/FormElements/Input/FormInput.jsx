/* eslint-disable react/prop-types */
import {
  FormErrorMessage,
  Input,
  InputGroup,
  InputRightElement,
} from "@chakra-ui/react";
import { Controller } from "react-hook-form";

const FormInput = ({
  control,
  required = false,
  name,
  inputProps = {},
  disabled = false,
  inputLeftElement,
  inputRightElement,
  defaultValue = "",
  placeholder = "",
  autoFocus = false,
  height,
  fontSize = "16px",
  setShowPass,
  showPass,
  type,
  ...props
}) => {
  return (
    <Controller
      name={name}
      control={control}
      defaultValue={defaultValue}
      rules={{ required }}
      render={({ field: { onChange, value }, fieldState: { error } }) => (
        <>
          <InputGroup {...props}>
            {inputLeftElement}

            <Input
              value={value}
              onChange={onChange}
              isInvalid={Boolean(error)}
              readOnly={disabled}
              placeholder={placeholder}
              autoFocus={autoFocus}
              {...inputProps}
              required={false}
              height={height}
              fontSize={fontSize}
              type={type}
            />
            <InputRightElement
              width="2.5rem"
              pt={1}
              pr={4}
              onClick={() => setShowPass(!showPass)}
            >
              {inputRightElement}
            </InputRightElement>
          </InputGroup>

          <FormErrorMessage>{error?.message}</FormErrorMessage>
        </>
      )}
    />
  );
};

export default FormInput;
