import {
  Button,
  Popover,
  PopoverBody,
  PopoverContent,
  PopoverTrigger,
} from "@chakra-ui/react";
import styles from "./styles.module.scss";
import FormInput from "components/FormElements/Input/FormInput";

const FilterSearch = ({title,name,control}) => {
  return (
    <div className={styles.filterSearchWrap}>
      <Popover placement="bottom-start">
        <PopoverTrigger>
          <Button className={styles.filerButton}><span>{title}:</span>Hammasi</Button>
        </PopoverTrigger>
        <PopoverContent>
          <PopoverBody>
          <FormInput  
            name={name}
            control={control}
            placeholder="Izlash..."
          />
          </PopoverBody>
        </PopoverContent>
      </Popover>
    </div>
  );
};

export default FilterSearch;
