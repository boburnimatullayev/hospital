import cls from "./styles.module.scss";
import {
  Popover,
  PopoverBody,
  PopoverContent,
  PopoverTrigger,
} from "@chakra-ui/react";
import clsx from "clsx";
import { useState } from "react";
import { FiDownload, FiEdit, FiMoreHorizontal, FiMoreVertical, FiTrash2 } from "react-icons/fi";

export const MiniPopover = ({
  callback = () => {},
  label="Удалить",
  type="delete",
  list=[],
  trigger,
  icon,
  placement = "auto",
  right,
  width = "300px",
  top,
  closeOnClick = true
}) => {
  const [isOpen, setIsOpen] = useState(false);

  const iconProps = {
    color: "#697E92",
    width: "20px",
    height: "20px",
  };

  const types = {
    delete: <FiTrash2 {...iconProps} />,
    edit: <FiEdit {...iconProps} />,
    download: <FiDownload {...iconProps} />,
    more: <FiMoreVertical {...iconProps} />,
    moreHorizontal: <FiMoreHorizontal {...iconProps} />,
  };

  return <Popover
    isOpen={isOpen}
    placement={"bottom"}
    returnFocusOnClose
    onClose={() => setIsOpen(false)}
    onOpen={() => setIsOpen(true)}
  >
    <PopoverTrigger>
      {
        trigger || types[type]
      }
    </PopoverTrigger>
    <PopoverContent
      position="absolute"
      width={width}
      // minW={}
      right={right ?? "100%"}
      padding="4px 16px"
      top={top ?? "100%"}
      display="block"
      sx={{ boxShadow: "0px 12px 16px -4px #10182814", outline: "none !important",borderRadius:"8px"  }}
   

    >
      {
        list?.length
          ? list.map((item, index) => {
            return (
              !item?.disabled ? <PopoverBody className={cls.body} padding="0" key={index}>
                <button
                  className={cls.btn}
                  onClick={() => {
                    item.callback();
                    closeOnClick && setIsOpen(false);
                  }}
                >
                  <div className={clsx(cls.btnInner, cls.withPadding)} >
                    <div className={clsx(cls.labelWrapper)}>
                      {
                        item?.icon && <span>{item.icon}</span>
                      }
                      <span style={{ color: item?.color }}>{item?.label}</span>
                    </div>
                    {item.component}
                  </div>
                </button>
              </PopoverBody>
              : null);
          }
          )
          : <PopoverBody padding="0" onClick={callback}>
            <span>
              <span className={cls.btnInner} >
                {
                  icon && <span>{icon}</span>
                }
                <span>{label}</span>
              </span>
            </span>
          </PopoverBody>
      }
    </PopoverContent>
  </Popover>;
};

let a = {
  user_id: "7cb078c7-1d24-40b3-a622-b5d3066e5ec3",
  is_active: true,
  password:
      "$argon2id$v=19$models=65536,t=3,p=4$JiZK4zlmHnkk7xHiPo/9/A$JfAxk4xP+5EfIZEVQ2pdQUZ59NQLN3usr/l11TApitY",
  image: {}
};
