import { ActionIcon } from "assets/icon";
import cls from "./style.module.scss";
import {
  IconButton,
  Popover,
  PopoverBody,
  PopoverContent,
  PopoverTrigger,
} from "@chakra-ui/react";
import { DndContext, closestCorners, KeyboardSensor,
    PointerSensor,
    useSensor,
    useSensors,} from "@dnd-kit/core";

  import { sortableKeyboardCoordinates, arrayMove } from "@dnd-kit/sortable";
import { useState } from "react";
import {
  SortableContext,
  verticalListSortingStrategy,
} from "@dnd-kit/sortable";
import clsx from "clsx";
import Drag from "modules/Patient/components/Drag/Drag";


export const DragPopover = ({
  width = "300px",
  columns,
  setColumns
}) => {
  const [isOpen, setIsOpen] = useState(false);



  const getTaskPos = (id) => columns.findIndex((task) => task.id === id);
  const handleDragEnd = (event) => {
    const { active, over } = event;
     if (active.id === over.id) return;
     setColumns((tasks) => {
      const originalPos = getTaskPos(active.id);
      const newPos = getTaskPos(over.id);

      return arrayMove(tasks, originalPos, newPos);
    });
  };

  const sensors = useSensors(
    useSensor(PointerSensor),
    useSensor(KeyboardSensor, {
      coordinateGetter: sortableKeyboardCoordinates,
    })
  );

  const types = {
    action: <ActionIcon />,
  };

  return (
    <Popover
      isOpen={isOpen}
      placement={"bottom"}
      returnFocusOnClose
      onClose={() => setIsOpen(false)}
      onOpen={() => setIsOpen(true)}
    >
      <PopoverTrigger>
        <IconButton variant="outline" icon={types.action} border={"none"} />
      </PopoverTrigger>
      <PopoverContent
        position="absolute"
        width={width}
        // minW={}
        right={right ?? "100%"}
        padding="4px 16px"
        top={top ?? "100%"}
        display="block"
        sx={{
          boxShadow: "0px 12px 16px -4px #10182814",
          outline: "none !important",
          borderRadius: "8px",
        }}
      >
        <PopoverBody className={cls.body}>
        <DndContext
        sensors={sensors}
        collisionDetection={closestCorners}
        onDragEnd={handleDragEnd}
      >
        <div
          style={{
            backgroundColor: "#f2f2f3",
            padding: "15px",
            width: "80%",
            maxWidth: "auto",
            display: "flex",
            flexDirection: "column",
            gap: "15px",
          }}
        >
          <SortableContext
            items={columns}
            strategy={verticalListSortingStrategy}
          >
            {columns.map((task) =>
              task.id !== 1 && task.id !== 8 ? (
                <>
                  <Drag key={task.id} id={task.id} title={task.title} />
                </>
              ) : (
                <></>
              )
            )}
          </SortableContext>
        </div>
      </DndContext>
        </PopoverBody>
      </PopoverContent>
    </Popover>
  );
};
