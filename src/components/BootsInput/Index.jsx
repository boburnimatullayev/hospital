import { DeleteIcon } from "@chakra-ui/icons";
import { Box, IconButton, Input } from "@chakra-ui/react";

const BootsInput = ({ onchange, valueInput, index, setValueINput }) => {
  return (
    <Box position={"relative"}>
      <Input
        defaultValue={valueInput}
        onChange={onchange}
        type="text"
        mb={7}
        size="lg"
        p={10}
        fontSize={"16px"}
        borderRadius={"8px"}
        border={"none"}
        backgroundColor={"#F6F8F9"}
      />
      <IconButton
        position={"absolute"}
        right={"20px"}
        top={"14px"}
        variant={"outline"}
        border={"none"}
        zIndex={1111}
        icon={<DeleteIcon boxSize={6} color={"red"} />}
        onClick={() => {
          setValueINput((old) => {
            return old.filter((v, i) => {
              return index !== i;
            });
          });
        }}
      />
    </Box>
  );
};

export default BootsInput;
