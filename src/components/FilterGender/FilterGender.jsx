import { Button, Menu, MenuButton, MenuItem, MenuList } from "@chakra-ui/react";
import styles from "./styles.module.scss";

const FilterGender = ({ title }) => {
  return (
    <div className={styles.filterGenderWrap}>
      <Menu>
        <MenuButton className={styles.filerButton} as={Button}>
          <p><span>{title}</span>: Hammasi</p>
        </MenuButton>
        <MenuList>
          <MenuItem>Erkak</MenuItem>
          <MenuItem>Ayol</MenuItem>
        </MenuList>
      </Menu>
    </div>
  );
};

export default FilterGender;
