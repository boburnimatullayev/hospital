import clsx from "clsx";
import cls from "./styles.module.scss";

export const IconButton = ({ children, className, onClick = () => {}, ...props }) => {
  return <button
    className={clsx(cls.btn, className)}
	 	onClick={(e) => {
      e.stopPropagation();
      onClick();
    }}
    {...props}
  >
    {children}
  </button>;
};
