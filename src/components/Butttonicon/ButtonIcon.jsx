import { Button } from "@chakra-ui/react";
import styles from "./style.module.scss";

const ButtonIcon = ({ children,onClick }) => {
  return <Button  onClick={onClick} className={styles.button_icon}>{children}</Button>;
};

export default ButtonIcon;
