/* eslint-disable react/prop-types */

import styles from "./FileModal.module.scss"

import { Button } from "@chakra-ui/react";

import { CloseIcon } from "@chakra-ui/icons";

const FileModal = ({ setOpenModal, getFilaInfo}) => {
  return (
    <div className={styles.background} onClick={()=>setOpenModal(false)}>
      <div 
        className={styles.container}
      >
        <span onClick={() => setOpenModal(false)} className={styles.closeBtn}>
          <CloseIcon />
        </span>
          
        <Button _active={{transform: 'translateY(1px)'}} onClick={getFilaInfo}>
          Готово
        </Button>
      </div>
    </div>
  );
};

export default FileModal;
