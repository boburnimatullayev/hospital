/* eslint-disable react/prop-types */
import {
  Box,
  Button,
  Flex,
  Icon,
  IconButton,
  Menu,
  MenuButton,
  MenuItem,
  MenuList,
  Modal,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Text,
  useDisclosure,
} from "@chakra-ui/react";
import clsx from "clsx";
import { useLocation, useNavigate } from "react-router-dom";
import { useState } from "react";
import logo from "../../assets/logo2.svg";
import user from "../../assets/png/userjpg.jpg";
import styles from "./index.module.scss";
import {
  ArrovPrewIcon,
  CloseSidebarIcon,
  FilterSettingIcon,
  LogoutIcon,
  OpenSidebarIcon,
  TringleIcon,
} from "assets/icon";
import { AiOutlineMore } from "react-icons/ai";
import authStore from "store/auth.store";

const Sidebar = ({ elements }) => {
  const navigate = useNavigate();
  const { pathname } = useLocation();
  const [open, setOpen] = useState(false);
  const { isOpen, onOpen, onClose } = useDisclosure();

  const onRowClick = (element) => {
    navigate(element?.link);
  };

  return (
    <Box className={open ? styles.sidebar_close : styles.sidebar}>
      <Box className={styles.header}>
        <Flex alignItems="center">
          <img src={logo} alt="logo" />
          <Text className={styles.logo}>{!open ? "Clinic Hero" : ""}</Text>
        </Flex>
      </Box>

      <Box className={styles.body}>
        <Box className={clsx(styles.row)}>
          <Box className={styles.element}>
            <Icon as={ArrovPrewIcon} className={styles.icon} />
            <p className={styles.arrowText}>Administratsiya</p>
          </Box>
        </Box>

        {elements?.map((element, index) => (
          <Box
            key={index}
            className={clsx(styles.row, {
              [styles.active]: pathname.startsWith(element.link),
            })}
            onClick={() => onRowClick(element)}
          >
            <Box className={styles.element}>
              <Icon as={element.icon} className={styles.icon} />
              <p className={styles.label}>{element.label}</p>
            </Box>
          </Box>
        ))}

        <Box className={clsx(styles.logoutWrap)}>
          <IconButton
            right={open ? "5" : 13}
            top={-20}
            zIndex={1}
            pos="absolute"
            variant="outline"
            border="none"
            onClick={() => setOpen(!open)}
            icon={open ? <CloseSidebarIcon /> :  <OpenSidebarIcon />}
          />
          <Box className={styles.element}>
            <img src={user} />
            <p className={styles.userText}>
              Ana de Armas <br /> <span>Administrator</span>
            </p>
            <Menu className={styles.menu} autoSelect={false}>
              <MenuButton
                as={IconButton}
                aria-label="Options"
                icon={<AiOutlineMore size={25} />}
                variant="unstyled"
                width={10}
                style={{ border: "none" }}
                className={styles.menuicon}
              />
              <MenuList className={styles.menuList}>
                <MenuItem className={styles.menuitem} icon={<TringleIcon />}>
                  Bildirishnomalar
                </MenuItem>
                <MenuItem
                  className={styles.menuitem}
                  icon={<FilterSettingIcon />}
                >
                  Sozlamalar
                </MenuItem>
                <MenuItem
                  style={{ color: "red" }}
                  className={styles.menuitem}
                  icon={<LogoutIcon />}
                  onClick={onOpen}
                >
                  Tizimdan chiqish
                </MenuItem>
              </MenuList>
            </Menu>
            <Modal isOpen={isOpen} onClose={onClose}>
              <ModalOverlay background="#10182899" />
              <ModalContent>
                <ModalHeader>Rostdan chiqishni xohlaysizmi?</ModalHeader>
                <ModalCloseButton />
                <ModalFooter>
                  <Button _active={{transform: 'translateY(1px)'}} mr={3} onClick={() => authStore.logout()}>
                    Ha
                  </Button>
                  <Button _active={{transform: 'translateY(1px)'}} variant="outline" onClick={onClose}>
                    Yo`q
                  </Button>
                </ModalFooter>
              </ModalContent>
            </Modal>
          </Box>
          <p className={styles.version}>version: 1.0.0</p>
        </Box>
      </Box>
    </Box>
  );
};
export default Sidebar;
