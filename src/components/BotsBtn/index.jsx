import { AddIcon } from "@chakra-ui/icons";
import { Box, Text,Button } from "@chakra-ui/react";


const BotsBtn = ({setValueINput,valueInput}) =>{
  return(
    <Box
    s="button"
    borderRadius="8px"
    p={5}
    width={"100%"}
    border={"2px"}
    borderStyle={"dashed"}
    borderColor={"#2196F3"}
    cursor={"pointer"}
    boxShadow={"0px 4px 22px 0px rgba(13, 42, 69, 0.08)"}
    onClick={() =>
      setValueINput((old) => {
        return [...old,   `` ];
      })
    }
  >
    <Text
      color={"#2196F3"}
      textAlign={"center"}
      fontSize={"18px"}
      fontWeight={"500"}
    >
      <AddIcon mr={3} />
      Добавить принципы и правила
    </Text>
  </Box>
  )
}


export default BotsBtn