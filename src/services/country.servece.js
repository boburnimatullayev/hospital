import { useQuery } from "react-query";
import httpRequest from "./httpRequest";

const authService = {
  getRegion: (params) => httpRequest.get("/api/region/all", params),
  getDestrict: (params) => httpRequest.get("/api/destrict/all", params),
};


export const useGetRegion = ({ params = {}, queryParams } = {}) => {
  return useQuery(
    ["GET_REGION", params],
    () => {
      return authService.getRegion(params);
    },
    queryParams
  );
};

export const useDestrict = ({ params = {}, queryParams } = {}) => {
    return useQuery(
      ["GET_DESTRICT", params],
      () => {
        return authService.getDestrict(params);
      },
      queryParams
    );
  };
  