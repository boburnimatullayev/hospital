import { useMutation, useQuery } from "react-query";
import httpRequestAuth from "./httpRequestAuth";

const authService = {
  login: (data) => httpRequestAuth.post("/api/auth/sign_in", data),
  getRole: (params) => httpRequestAuth.get("/api/role_for_ui", params),
};

export const useLoginMutation = (mutationSettings) => {
  return useMutation((data) => authService.login(data), mutationSettings);
};

export const GetUserRole = ({ params = {}, queryParams } = {}) => {
  return useQuery(
    ["GET_USER_ROLE", params],
    () => {
      return authService.getUserRole(params);
    },
    queryParams
  );
};
