import { Route, Routes } from "react-router-dom";
import Analysis from "./Analysis/analysis";

export const AnalysisRoutes = () => {
  return (
    <Routes>
      <Route path="" element={<Analysis />} />
    </Routes>
  );
};
