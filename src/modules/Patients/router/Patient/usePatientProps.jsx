import { useState } from "react";

import styles from "./style.module.scss";
import {
  KeyboardSensor,
  PointerSensor,
  useSensor,
  useSensors,
} from "@dnd-kit/core";
import { sortableKeyboardCoordinates, arrayMove } from "@dnd-kit/sortable";
import { IconButton } from "@chakra-ui/react";
import { ActionIcon } from "assets/icon";
import { useEffect } from "react";
import { PatientPopover } from "modules/Patients/components/PatientPopover";
import CheckboxDefault from "components/FormElements/HtmlChekboxDefault/CheckboxDefault";
import { useForm } from "react-hook-form";

export const usePatientProps = () => {
  const {control} = useForm()
  const handleOpen = () => {
    setOpenDrag(!openDrag);
  };
  const [openDrag, setOpenDrag] = useState(false);
  const [checkboxParent, setCheckboxParent] = useState(false);

  useEffect(() => {
    window.addEventListener("click", (e) => {
      if (e.target.className.animVal) {
        setOpenDrag(true);
      } else {
        setOpenDrag(false);
      }
    });
  }, [openDrag]);

  const [columns, setColumns] = useState([
    {
      id: 1,
      title: (
        <CheckboxDefault
          onChange={(e) =>
            e.target.checked
              ? setCheckboxParent(true)
              : setCheckboxParent(false)
          }
        />
      ),
      dataIndex: "name",
      key: "name",
      render: () => (
        <>
          <CheckboxDefault
            isChecked={checkboxParent}
            onChange={(e) => console.log(e.target.checked)}
          />
        </>
      ),
    },
    {
      id: 2,
      title: <p>F.I.O</p>,
      dataIndex: "name",
      key: "name",
    },
    {
      id: 3,
      title: "Jinsi",
      dataIndex: "gender",
      key: "gender",
    },
    {
      id: 4,
      title: "Viloyat",
      dataIndex: "region",
      key: "region",
    },
    {
      id: 5,
      title: "Tug’ilgan sana",
      dataIndex: "date",
      key: "date",
    },
    {
      id: 6,
      title: "Pasport seriya №",
      dataIndex: "passport",
      key: "passport",
    },
    {
      id: 7,
      title: "Telefon raqam",
      dataIndex: "phone",
      key: "phone",
    },
    {
      id: 8,
      title: (
        <IconButton
          onClick={handleOpen}
          border={"none"}
          variant="outline"
          icon={<ActionIcon />}
        />
      ),
      render: (e, data) => <PatientPopover row={data} />,
    },
  ]);

  const data = [
    {
      id: 1,
      name: `Bobur Nimatullayev`,
      gender: `Erkak`,
      region: `Samarqand`,
      date: `15.12.2003`,
      passport: `AC1488502`,
      phone: `+998 (99) 123 45 67`,
    },
    {
      id: 2,
      name: `Jamshidbek Abdullayev`,
      gender: `Erkak`,
      region: `Samarqand`,
      date: `15.12.2003`,
      passport: `AC1488502`,
      phone: `+998 (99) 123 45 67`,
    },
    {
      id: 3,
      name: `Jamshidbek Abdullayev`,
      gender: `Erkak`,
      region: `Samarqand`,
      date: `15.12.2003`,
      passport: `AC1488502`,
      phone: `+998 (99) 123 45 67`,
    },
  ];

  const getTaskPos = (id) => columns.findIndex((task) => task.id === id);
  const handleDragEnd = (event) => {
    const { active, over } = event;

    if (active.id === over.id) return;

    setColumns((tasks) => {
      const originalPos = getTaskPos(active.id);
      const newPos = getTaskPos(over.id);

      return arrayMove(tasks, originalPos, newPos);
    });
  };
  const sensors = useSensors(
    useSensor(PointerSensor),
    useSensor(KeyboardSensor, {
      coordinateGetter: sortableKeyboardCoordinates,
    })
  );
  return {
    columns,
    data,
    handleDragEnd,
    sensors,
    openDrag,
    control
  };
};
