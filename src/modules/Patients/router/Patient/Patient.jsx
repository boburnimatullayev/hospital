import { Button } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";
import styles from "./style.module.scss";
import DataTable from "components/DataTable";
import { usePatientProps } from "./usePatientProps";
import { DndContext, closestCorners } from "@dnd-kit/core";
import {
  SortableContext,
  verticalListSortingStrategy,
} from "@dnd-kit/sortable";
import Drag from "components/Drag/Drag";
import Header, { HeaderLeftSide, HeaderTitle } from "components/Header";
import FilterSearch from "components/FilterSearch/FilterSearch";
import FilterGender from "components/FilterGender/FilterGender";

const Patient = () => {
  const { columns, data, handleDragEnd, sensors, openDrag, control } =
    usePatientProps();

  const navigate = useNavigate();

  return (
    <div className={styles.qr_wrap}>
      <Header>  
        <HeaderLeftSide>
          <HeaderTitle>Bemorlar</HeaderTitle>
        </HeaderLeftSide>

        <HeaderLeftSide>
          <Button
            _active={{ transform: "translateY(1px)" }}
            onClick={() => navigate("/patient/add-patient")}
            borderRadius="8px"
            height="44px"
            background="#4D4AEA"
          >
            Yangi bemor
          </Button>
        </HeaderLeftSide>
      </Header>

      <div className={styles.contendCon}>
        <div className={styles.filterWrap}>
          <FilterSearch control={control} name="fullname" title="F.I.O" />
          <FilterGender title="Jinsi" />
          <FilterSearch control={control} name="reion" title="Viloyat" />
          <FilterSearch
            control={control}
            name="passport"
            title="Pasport seriya"
          />
          <FilterSearch control={control} name="phone" title="Telefon raqam" />
        </div>
        <div className={styles.contend}>
          <DataTable columns={columns} data={data} />
          {openDrag ? (
            <div className={styles.dragWrap}>
              <DndContext
                sensors={sensors}
                collisionDetection={closestCorners}
                onDragEnd={handleDragEnd}
              >
                <div className={styles.dragContend}>
                  <SortableContext
                    items={columns}
                    strategy={verticalListSortingStrategy}
                  >
                    {columns.map((task) =>
                      task.id !== 1 && task.id !== 8 ? (
                        <Drag key={task.id} id={task.id} title={task.title} />
                      ) : (
                        <></>
                      )
                    )}
                  </SortableContext>
                </div>
              </DndContext>
            </div>
          ) : (
            <></>
          )}
        </div>
      </div>
    </div>
  );
};

export default Patient;
