import { Route, Routes } from "react-router-dom";
import PatientCreate from "./PatientCreate/PatientCreate";
import Patient from "./Patient/Patient";



export const PatientRoutes = () => {
  return <Routes>
    <Route path="" element={<Patient/>} />
    <Route path="/add-patient" element={<PatientCreate />} />
  </Routes>;
};
