import Header, { HeaderLeftSide, HeaderTitle } from "components/Header";
import { Button } from "@chakra-ui/react";
import styles from "./style.module.scss";
import { Accordion } from "modules/Patients/components/Accardion";
import { usePatientCreteProps } from "./usePatientCreteProps";
import PatientFrom from "modules/Patients/components/PatientFrom";
import { DynamicSelectReview } from "modules/Patients/components/DynamicSelectReview";
import ButtonIcon from "components/Butttonicon/ButtonIcon";
import { PlusIcon, PrintIcon } from "assets/icon";
import { DynamicSelectAnalysis } from "modules/Patients/components/DynamicSelectAnalysis";
const PatientCreate = () => {
  const {
    review,
    control,
    patient,
    analysis,
    selectsFormR,
    selectsFormA,
    handleClosePatient,
    handleCloseReview,
    handleClosAnalysis,
    handleButtonClickReview,
    handleDeleteSelectReview,
    handleButtonClickAnalysis,
    handleDeleteSelectAnalysis,
    handleSubmit,
    onSubmit,
    region,
    destrictArr
  } = usePatientCreteProps();

  return (
    <div>
      <Header>
        <HeaderLeftSide>
          <HeaderTitle>Yangi bemor qo’shish</HeaderTitle>
        </HeaderLeftSide>

        <HeaderLeftSide>

          <Button  _active={{transform: 'translateY(1px)'}} onClick={handleSubmit(onSubmit)}>Yaratish</Button>

        </HeaderLeftSide>
      </Header>

      <div className={styles.container}>
        <div style={{ width: "100%" }}>
          <Accordion
            handleClose={handleClosePatient}
            title={"Bemorlar"}
            open={patient}
          >
            <PatientFrom styles={styles} control={control} region={region} destrictArr={destrictArr}/>
          </Accordion>
        </div>
        <div style={{ width: "100%"}}>
          <Accordion
            handleClose={handleCloseReview}
            title={"Ko’rik"}
            open={review}
          >
            <div className={styles.review_form}>
              {/* {selectsFormR.map((item) => ( */}
                <>
                  <DynamicSelectReview
                    styles={styles}
                    // key={item.id}
                    // id={item.id}
                    control={control}
                    onDelete={handleDeleteSelectReview}
                    option={[]}
                    
                  />
                </>
              {/* ))} */}
            </div>
            {selectsFormR.length >= 0 && (
              <ButtonIcon onClick={handleButtonClickReview}>
                <span>Chek chiqarish</span> <PrintIcon />
              </ButtonIcon>
            )}
            <ButtonIcon onClick={handleButtonClickReview}>
              <span>Yaratish</span> <PlusIcon />
            </ButtonIcon>
          </Accordion>


          <Accordion
            handleClose={handleClosAnalysis}
            title={"Analiz"}
            open={analysis}
          >
            <div className={styles.review_form}>
              {selectsFormA.map((item) => (
                <>
                  <DynamicSelectAnalysis
                    styles={styles}
                    key={item.id}
                    id={item.id}
                    control={control}
                    onDelete={handleDeleteSelectAnalysis}
                    option={item.option}
                    
                  />
                </>
              ))}
            </div>
            {selectsFormA.length > 0 && (
              <ButtonIcon onClick={handleButtonClickAnalysis}>
                <span>Chek chiqarish</span> <PrintIcon />
              </ButtonIcon>
            )}
            <ButtonIcon onClick={handleButtonClickAnalysis}>
              <span>Yaratish</span> <PlusIcon />
            </ButtonIcon>
          </Accordion>
        </div>
      </div>
    </div>
  );
};

export default PatientCreate;
