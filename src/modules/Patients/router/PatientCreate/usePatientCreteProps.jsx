import { useMemo, useState } from "react";
import { useForm } from "react-hook-form";
import { useDestrict, useGetRegion } from "services/country.servece";

export const usePatientCreteProps = () => {
  const [patient, setPatient] = useState(true);
  const [review, setReview] = useState(false);
  const [analysis, setAnalysis] = useState(false);
  const [selectsFormR, setSelectsFormR] = useState([]);
  const [selectsFormA, setSelectsFormA] = useState([]);

  const { control, handleSubmit,watch } = useForm();

  const {data:region} = useGetRegion()
  const {data:destrict} = useDestrict()

  const destrictArr = useMemo(() => {
      if(watch("country")){
        return destrict.filter(item => item?.regionId === watch("country"))
      }
  },[watch("country")])
   console.log("data",region)
   console.log("data",destrictArr)

  const handleClosePatient = () => {
    setPatient(!patient);
  };
  const handleCloseReview = () => {
    setReview(!review);
  };
  const handleClosAnalysis = () => {
    setAnalysis(!analysis);
  };
  const handleButtonClickReview = () => {
    setSelectsFormR([...selectsFormR, { option: [], id: Date.now() }]);
  };

  const handleButtonClickAnalysis = () => {
    setSelectsFormA([...selectsFormA, { option: [], id: Date.now() }]);
  };
  const handleDeleteSelectReview = (id) => {
    const newSelectsReview = selectsFormR.filter((item) => item.id !== id);
    setSelectsFormR(newSelectsReview);
  };

  const handleDeleteSelectAnalysis = (id) => {
    const newSelectsAnalysis = selectsFormA.filter((item) => item.id !== id);
    setSelectsFormA(newSelectsAnalysis);
  };

  const onSubmit = (data) => {
    console.log("data", data);
  };

  return {
    review,
    control,
    patient,
    analysis,
    selectsFormR,
    selectsFormA,
    handleClosePatient,
    handleCloseReview,
    handleClosAnalysis,
    handleButtonClickReview,
    handleDeleteSelectReview,
    handleButtonClickAnalysis,
    handleDeleteSelectAnalysis,
    handleSubmit,
    onSubmit,
    region,
    destrictArr
  };
};
