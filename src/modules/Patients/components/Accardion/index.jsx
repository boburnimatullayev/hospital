import { ArrowBottomIcon, ArrowTopIcon } from "assets/icon";
import styles from "./style.module.scss";
import { IconButton } from "@chakra-ui/react";

export const Accordion = ({ children, title, handleClose, open }) => {
  return (
    <div className={styles.accordion_wrap}>
      <div className={styles.accordion_header}>
        <h2 className={styles.accordion_title}>{title}</h2>
        <div className={styles.accordion_Aaction}>
          <IconButton
            className={open ? styles.iconOpen : styles.iconClose}
            onClick={handleClose}
            variant="outline"
            border="none"
            icon={<ArrowTopIcon />}
          />
        </div>
      </div>
      <div
       className={  open ?  styles.accordion_body : styles.accordion_body_close}
      >{children}</div>
    </div>
  );
};

// export const AccordionHeader = ({children}) => {
//     return(
//         <div className={styles.accordion_wrap}>
//                {children}
//         </div>
//     )
// }
