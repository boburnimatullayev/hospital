import { IconButton } from "@chakra-ui/react";
import { CloseXIcon } from "assets/icon";
import FormRow from "components/FormElements/FormRow";
import FormSelect from "components/FormElements/Select/FormSelect";

export const DynamicSelectReview = ({ option, onDelete, control, id, styles }) => {
  return (
    <>
      <FormRow label="Bo’lim">
        <FormSelect
          placeholder="Bo’limni tanlang"
          name="bolim"
          control={control}
          options={option}
        />
      </FormRow>
      <FormRow label="Shifokor">
        <div className={styles.review_form_action}>
          <FormSelect
            //   style={{width:"100%"}}
            placeholder="Shifokorni tanlang"
            name="bolim"
            control={control}
            options={[]}
          />
          {/* <IconButton
            onClick={() => onDelete(id)}
            variant="outline"
            border="none"
            icon={<CloseXIcon />}
          /> */}
        </div>
      </FormRow>
    </>
  );
};
