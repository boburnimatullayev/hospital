import { IconButton } from "@chakra-ui/react";
import { CloseXIcon } from "assets/icon";
import FormRow from "components/FormElements/FormRow";
import FormInput from "components/FormElements/Input/FormInput";
import FormSelect from "components/FormElements/Select/FormSelect";

export const DynamicSelectAnalysis = ({ option, onDelete, control, id, styles }) => {
  return (
    <>
      <FormRow label="Analiz turi*">
        <FormSelect
          placeholder="Analiz turini tanlang"
          name={`analysis[name]`}
          control={control}
          options={option}
        />
      </FormRow>
      <FormRow label="Analiz narxi">
        <div className={styles.review_form_action}>
          <FormInput
            placeholder="68 500 so’m"
            name={`analysis.name`}
            control={control}
            // options={[]}
          />
          <IconButton
            onClick={() => onDelete(id)}
            variant="outline"
            border="none"
            icon={<CloseXIcon />}
          />
        </div>
      </FormRow>
    </>
  );
};
