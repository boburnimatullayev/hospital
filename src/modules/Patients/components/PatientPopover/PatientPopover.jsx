import { IconButton } from "components/IconButton";
import { MiniPopover } from "components/MiniPopover";
import { FiEdit, FiTrash2 } from "react-icons/fi";

export const PatientPopover = ({
  handleEdit,
  handleDelete,
  row,
}) => {
  return <IconButton>
    <MiniPopover
      type="more"
      list={
        [
          {
            icon: <FiEdit color="#2A2A2A" /> ,
            label: "O’zgartirish",
            color: "#2A2A2A",
            callback: () => handleEdit(row?.id)
          },
          {
            icon: <FiTrash2 color="#FF0000" /> ,
            label: "O’chirish",
            color: "#FF0000",
            callback: () => handleDelete(row?.id),
          },
        ]
      } />
  </IconButton>;
};
