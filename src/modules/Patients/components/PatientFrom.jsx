import { Radio, RadioGroup } from "@chakra-ui/react";
import FormRow from "components/FormElements/FormRow";
import FormInput from "components/FormElements/Input/FormInput";
import FormPhoneInput from "components/FormElements/Input/FormPhoneInput";
import FormSelect from "components/FormElements/Select/FormSelect";
import { Controller } from "react-hook-form";

const PatientFrom = ({ styles, control, region,destrictArr }) => {
  return (
    <div className={styles.patient_form}>
      <FormRow label="Ism">
        <FormInput
          name="name"
          control={control}
          borderRadius="8px"
          height="44px"
          placeholder="Bemor ismini kiriting"
          marginBottom="2px"
        />
      </FormRow>
      <FormRow label="Familiya">
        <FormInput
          name="fullName"
          control={control}
          borderRadius="8px"
          height="44px"
          placeholder="Bemor familiyasini kiriting"
          marginBottom="2px"
        />
      </FormRow>
      <FormRow label="Sharif">
        <FormInput
          name="fullName2"
          control={control}
          borderRadius="8px"
          height="44px"
          placeholder="Bemor sharifini kiriting"
          marginBottom="2px"
        />
      </FormRow>
      <FormRow label="Tug’ilgan sana">
        <FormInput
          name="fullName2"
          control={control}
          borderRadius="8px"
          height="44px"
          placeholder="Tug'ilgan sanani tanlang"
          marginBottom="2px"
        />
      </FormRow>
      <FormRow label="Viloyat">
        <FormSelect
          placeholder="Viloyatni tanlang"
          name="country"
          control={control}
          options={region?.map((item) => ({
            label: item.nameOz,
            value: item.id,
          }))}
        />
      </FormRow>
      <FormRow label="Tuman">
        <FormSelect
          placeholder="Tumanni tanlang"
          name="district"
          control={control}
          options={destrictArr?.map((item) => ({
            label: item.nameOz,
            value: item.id,
          }))}
        />
      </FormRow>
      <FormRow label="Telefon raqam">
        <FormPhoneInput
          name="phone"
          control={control}
          borderRadius="8px"
          height="44px"
          placeholder="+998 99-999-99-99"
          marginBottom="2px"
        />
      </FormRow>
      <FormRow label="Passport seriya">
        <div style={{ display: "flex", gap: "10px" }}>
          <FormInput
            name="passport"
            control={control}
            borderRadius="8px"
            width="200px"
            height="44px"
            placeholder="AB"
            inputProps={{ max: "2" }}
            marginBottom="2px"
          />
          <FormInput
            name="passport2"
            control={control}
            borderRadius="8px"
            height="44px"
            placeholder="1234567"
            inputProps={{ max: "2" }}
            marginBottom="2px"
          />
        </div>
      </FormRow>
      <FormRow label="Tuman">
        <FormSelect
          placeholder="Tumanni tanlang"
          name="country2"
          control={control}
          options={[]}
        />
      </FormRow>
      <FormRow label="Jinsi">
        <Controller
          name="gender"
          control={control}
          // defaultValue="erkak"
          render={({ field }) => (
            <RadioGroup style={{ display: "flex", gap: "10px" }} {...field}>
              <Radio value="male">Erkak</Radio>
              <Radio value="female">Ayol</Radio>
            </RadioGroup>
          )}
        />
      </FormRow>
    </div>
  );
};

export default PatientFrom;
