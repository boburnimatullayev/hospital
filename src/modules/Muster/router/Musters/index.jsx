import { Button } from "@chakra-ui/react";
import Header, {
  HeaderExtraSide,
  HeaderLeftSide,
  HeaderTitle,
} from "components/Header";
import { useNavigate } from "react-router-dom";
import cls from "./style.module.scss";
import SearchInput from "components/FormElements/Input/SearchInput";
import { AddIcon, CloseIcon } from "@chakra-ui/icons";
import { CloseXIcon } from "assets/icon";
import { SearchIcon } from "@chakra-ui/icons";
import { useState } from "react";
import DataTable from "components/DataTable";
import { usePatientProps } from "modules/Patients/router/Patient/usePatientProps";

const Muster = () => {
  const navigate = useNavigate();
  const [visibleSearchInput, setVisibleSearchInput] = useState(true);
  const { columns, data, handleDragEnd, sensors, openDrag } = usePatientProps();

  return (
    <div className={cls.muster}>
      <Header>
        <HeaderLeftSide>
          <HeaderTitle>Ko'rik</HeaderTitle>
        </HeaderLeftSide>

        <HeaderExtraSide alignSelf="right"></HeaderExtraSide>
        <HeaderExtraSide>
          <SearchInput
            placeholder="Passport seriya, viloyat, telefon raqam"
            fontSize="14px"
            lineHeight="18px"
            fontWeight="500"
            width="333px"
            height="40px"
            background="#fff"
            marginRight="-55px"
            visibleSearchInput={visibleSearchInput}
          />
          <Button
            _hover={{ bg: "#fff" }}
            sx={{
              bg: "#fff",
              width: "40px",
              height: "40px"
            }}
            onClick={() => setVisibleSearchInput(!visibleSearchInput)}
          >
            {visibleSearchInput ? (
              <CloseIcon fontSize={12} color={"black"}  />
            ) : (
              <SearchIcon fontSize={16} color={"black"}/>
            )}
          </Button>
          <Button
            onClick={() => navigate("")}
            _hover={{ bg: "#5755e6" }}
            _active={{ transform: "translateY(1px)" }}
            sx={{
              display: "flex",
              gap: "12px",
              borderRadius: "8px",
              height: "40px",
              width: visibleSearchInput ? "190px" : "133.1px",
              background: "#4D4AEA",
            }}
          >
            <AddIcon /> <span> Yangi ko'rik</span>
          </Button>
        </HeaderExtraSide>
      </Header>

      <div className={cls.table}>
        <DataTable columns={columns} data={data} />
      </div>
    </div>
  );
};

export default Muster;
