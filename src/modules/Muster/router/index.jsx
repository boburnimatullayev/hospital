import { Route, Routes } from "react-router-dom"
import Muster from "./Musters"

export const MusterRoute = () => {
  return (
    <Routes>
      <Route path="" element={<Muster />} />
    </Routes>
  )
}
