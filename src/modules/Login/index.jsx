import { Box, Button, Heading } from "@chakra-ui/react";
import FormRow from "components/FormElements/FormRow";
import FormInput from "components/FormElements/Input/FormInput";
// import useCustomToast from 'hooks/useCustomToast';
import { useForm } from "react-hook-form";
// import { useLoginMutation } from "../../services/auth.service";
import authStore from "../../store/auth.store";

import { useNavigate } from "react-router-dom";
// import styles from './index.module.scss';

import cls from "./index.module.scss";
import { BsEyeSlashFill, BsEyeFill } from "react-icons/bs";
import { useState } from "react";
import { useLoginMutation } from "services/auth.service";
import httpRequestAuth from "services/httpRequestAuth";

const Login = () => {
  const [showPass, setShowPass] = useState(false);
  const navigate = useNavigate();
  const form = useForm();

  const { mutate: login, isLoading } = useLoginMutation({
    onSuccess: (data) => {
      authStore.login(data);
      if (authStore.token.length > 0) {
        httpRequestAuth.get("/api/role_for_ui").then((res) => {
          const role = res.slice(1, res.length - 1);
          authStore.updateUserRole(role);
          navigate("/");
        });
      }
    },
  });

  const onSubmit = (values) => {
    login({ ...values });
  };

  return (
    <Box pt="10px">
      <Heading textAlign="center" fontSize="32px" mb="40px">
        Tizimga kirish
      </Heading>

      <FormRow label="Login">
        <FormInput
          name="username"
          control={form.control}
          width="369px"
          borderRadius="8px"
          height="44px"
          placeholder="Login"
          marginBottom="30px"
          required
        />
      </FormRow>
      <FormRow label="Password">
        <FormInput
          name="password"
          required
          control={form.control}
          width="369px"
          borderRadius="8px"
          height="44px"
          placeholder="Password"
          marginBottom="15px"
          setShowPass={setShowPass}
          showPass={showPass}
          inputRightElement={
            showPass ? (
              <BsEyeFill color="#b0b0b0" size={20} />
            ) : (
              <BsEyeSlashFill color="#b0b0b5" size={20} />
            )
          }
          type={showPass ? "text" : "password"}
        />
      </FormRow>
      <p className={cls.forgotPassword}>Parolni unutdingizmi?</p>
      <Button
        onClick={form.handleSubmit(onSubmit)}
        isLoading={isLoading}
        width="369px"
        borderRadius="8px"
        height="44px"
        background="#4D4AEA"
        type="submit"
        _active={{transform: 'translateY(1px)'}}
      >
        Kirish
      </Button>
    </Box>
  );
};

export default Login;
