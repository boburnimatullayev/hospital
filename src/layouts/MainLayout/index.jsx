import { Box, Flex } from "@chakra-ui/react";
import Sidebar from "../../components/Sidebar";
import styles from "./index.module.scss";
import { FaRegUserCircle } from "react-icons/fa";
import { RiCheckboxMultipleLine } from "react-icons/ri";
import { TbDeviceAnalytics } from "react-icons/tb";

import { Outlet } from "react-router-dom";

const elements = [
  {
    label: "Bemorlar",
    icon: FaRegUserCircle,
    link: "/patient",
  },
  {
    label: "Ko’rik",
    icon: RiCheckboxMultipleLine,
    link: "/muster",
  },
  {
    label: 'Analiz',
    icon: TbDeviceAnalytics,
    link: '/analysis'
  },
];

const MainLayout = () => {
  return (
    <Flex  className={styles.mainLayout}>
      <Sidebar elements={elements} />

      <Box flex={1} overflowX="hidden">
        <div className={styles.BoxWrap}>
          <Outlet />
        </div>
      </Box>
    </Flex>
  );
};
export default MainLayout;
