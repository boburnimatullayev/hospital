import { Box } from "@chakra-ui/react";
import { Outlet } from "react-router-dom";
import styles from "./index.module.scss";
import { FullLogoIcon } from "assets/icon";

const AuthLayout = () => {
  return (
    <Box className={styles.layout}>
      <Box fontWeight="bold" className={styles.logo}>
        <FullLogoIcon />
      </Box>
      <Box className={styles.card}>
        <Box className={styles.formSide}>
          <Outlet />
        </Box>
      </Box>
    </Box>
  );
};

export default AuthLayout;
