import MainLayout from "layouts/MainLayout";
import { MusterRoute } from "modules/Muster/router";
import { PatientRoutes } from "modules/Patients/router";
import {AnalysisRoutes} from "modules/Analysis/router"


export const ROLE_MANAGER = [
  {
    path: "/",
    element: <MainLayout />,
    lazy: true,
    children: [
      {
        path: "patient/*",
        element: <PatientRoutes />
      },
      {
        path: "muster",
        element: <MusterRoute />
      },
      {
        path: "analysis",
        element: <AnalysisRoutes />
      },
    ],
  },
];
