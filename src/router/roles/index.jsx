export { ROLE_MANAGER } from "./role-manager";
export { ROLE_REGISTRATION } from "./role-registration";
export { ROLE_CASHIER } from "./role-cashier";
export { ROLE_SUPER_REGISTRATION } from "./role-super-registration";
export { ROLE_DOCTOR } from "./role-doctor";
export { ROLE_DIRECTOR } from "./role-director";
export { ROLE_SUPER_ADMIN } from "./role-super-admin";
