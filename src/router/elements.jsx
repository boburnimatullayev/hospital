import { useRoutes } from "react-router-dom";
import {
  ROLE_MANAGER,
  // ROLE_REGISTRATION,
  // ROLE_CASHIER,
  // ROLE_SUPER_REGISTRATION,
  // ROLE_DOCTOR,
  // ROLE_DIRECTOR,
  // ROLE_SUPER_ADMIN,
} from "./roles";
import authStore from "store/auth.store";

export const Elements = () => {
  const roleName = authStore.user_role;

  const routes = {
    ROLE_MANAGER,
    // ROLE_REGISTRATION,
    // ROLE_CASHIER,
    // ROLE_SUPER_REGISTRATION,
    // ROLE_DOCTOR,
    // ROLE_DIRECTOR,
    // ROLE_SUPER_ADMIN,

  };

  const elements = useRoutes(routes[roleName]);

  return elements;
};
