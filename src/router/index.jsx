import { observer } from "mobx-react-lite";
import { Navigate, Route, Routes } from "react-router-dom";
import AuthLayout from "../layouts/AuthLayout";
import Login from "../modules/Login";

import authStore from "store/auth.store";
import { Suspense } from "react";
import { Elements } from "./elements";

const Router = () => {
  const { user_role } = authStore;

  if (!user_role)
    return (
      <Routes>
        <Route path="/" element={<AuthLayout />}>
          <Route index element={<Navigate to="/login " />} />
          <Route path="login" element={<Login />} />
          <Route path="*" element={<Navigate to="/login" />} />
        </Route>
        <Route path="*" element={<Navigate to="/login" />} />
      </Routes>
    );

  return (
    <Suspense
      fallback={
        <div className="preloader">
          <span className="preloader-element"></span>
        </div>
      }
    >
      {!Elements() ? (
        <Routes>
          <Route path="/" element={<p>no data</p>} />
          <Route path="*" element={<Navigate to="/patient" />} />
        </Routes>
      ) : (
        <Elements />
      )}
    </Suspense>
  );
};

export default observer(Router);

{/* <Routes>
  <Route path='/' element={<MainLayout />}>
    <Route index element={<Navigate to='patient' />} />
    <Route path='/patient' element={<Patient />} />
  </Route>
</Routes> */}
